{
  let str = "str";
  let num = 1;
  let bool = true;

  let func = () => {};
  let obj = {};

  console.log(str, num, bool, func(), obj);

  //   str = 1;
  //   num = "str";
  //   bool = null;
}

{
  class Base {
    num = 1;
    str: string;
    regExpOptional?: RegExp;

    constructor(str: string) {
      this.str = str;
    }

    hello(): string {
      return `Hello ${this.str}`;
    }

    greeting(name: string) {
      return `Hi! ${name}`;
    }

    get regExp() {
      if (!this.regExpOptional) {
        return new RegExp("test");
      }

      return this.regExpOptional;
    }
  }

  class Inherit extends Base {
    greeting(name: string) {
      return `${super.greeting(name)}. How are you?`;
    }
  }

  const base = new Base("world");
  const inherit = new Inherit("my world");

  console.log(base.hello());
  console.log(base.greeting("TypeScript"));
  console.log(inherit.greeting("TypeScript"));
}

{
  class Base {
    a = "a";
    public b = "b";
    protected c = "c";
    private d = "d";

    method() {
      this.d;
    }
  }

  class Inherit extends Base {
    method() {
      this.a;
      this.b;
      this.c;
      // this.d;
    }
  }

  const base = new Base();
  const inherit = new Inherit();
  console.log(base.a);
  console.log(base.b);
  // console.log(base.c);
  // console.log(base.d);
  console.log(inherit.a);
  console.log(inherit.b);
  // console.log(inherit.c);
  // console.log(inherit.d);
}

{
  class BaseA {
    constructor(private str: string) {
      this.str = str;
    }

    method() {
      console.log(this.str);
    }
  }

  class BaseB {
    str: string;
    constructor(str: string) {
      this.str = str;
    }
  }

  console.log(new BaseA("A"));
  console.log(new BaseB("B"));
}

{
  abstract class Animal {
    abstract name: string;
    abstract get poo(): string;

    abstract speak(): string;

    sleep(): string {
      return "zzzZZZ...";
    }
  }

  class Cat extends Animal {
    name = "Cat";
    poo = "Poo";

    speak(): string {
      return "meow";
    }
  }

  console.log(new Cat());
}

{
  function hello(word: string): string {
    return `Hello, ${word}`;
  }
  console.log(hello("world"));

  function bye(word: string) {
    return `Bye, ${word}`;
  }
  console.log(bye("world"));

  function hey(word?: string) {
    return `Hey, ${word || "TypeScript"}`;
  }
  console.log(hey());
  console.log(hey("world"));

  function ahoy(word = "TypeScript") {
    return `Ahoy! ${word}`;
  }
  console.log(ahoy());

  function funcA(arg1: string, arg2?: string) {
    return `Hello, ${arg1}, ${arg2}`;
  }
  console.log(funcA("A", "B"));

  function funcB(arg: string, ...args: string[]) {
    return `Hello, ${arg}, ${args.join(".")}`;
  }
  console.log(funcB("A", "B", "C"));
}

{
  function asyncModoki(callback: (value: string) => void) {
    callback("TypeScript");
  }

  asyncModoki((value: string): void => console.log(`Hello, ${value}`));
}

{
  function returnByPromise(word: string) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(word);
      }, 100);
    });
  }

  async function helloAsync(): Promise<string> {
    console.log("A");
    const word = await returnByPromise("B");
    console.log(word);
    console.log("C");

    return "D";
  }

  (async () => {
    const hello = await helloAsync();
    console.log(hello);
  })();

  // helloAsync().then(hello => console.log(hello));
}

// オブジェクト型リテラルは、JavaScriptのオブジェクトリテラルに似た記法で、匿名の型を作り出す機能です
// オブジェクト型リテラルは型を指定する箇所*1であればどこでも使えます
{
    var objA = {
        x: 1,
        y: 2
    };
    var objB = void 0;
    objB = { x: 1, y: 2 };
    console.log(objA);
    console.log(objB);
    function move(value, delta) {
        if (delta.dx) {
            value.x += delta.dx;
        }
        if (delta.dy) {
            value.y += delta.dy;
        }
        return value;
    }
    var result = move({ x: 1, y: 2 }, { dx: 3 });
    console.log(result);
}
// Property Signatures
{
    var obj = void 0;
    obj = {
        property: "Hi!"
    };
    console.log(obj.property);
}
// Call Signatures
{
    var func = void 0;
    func = function (word) {
        return "Hello " + word + "!";
    };
    console.log(func("world"));
}
// Construct SignaturesZ
{
    var myClass = void 0;
    myClass = /** @class */ (function () {
        function myClass() {
        }
        return myClass;
    }());
    console.log(new myClass());
}
// Index Signatures
// インデックスシグニチャの利用は静的な検証の恩恵を受けられない危険性が高いため、安易に使わないようにしましょう。
{
    var objA = {};
    // どういった使い方ができるの？
    var s1 = objA[1];
    var objB = {};
    var s3 = objB[1];
    var s4 = objB["test"];
    objA = {
        0: "str"
    };
    var tmp = {
        0: "str",
        str: "str"
    };
    objA = tmp;
    objB = {
        0: "str",
        str: "str"
    };
    console.log(s1, s3, s4);
}
// Method Signatures
{
    var obj = void 0;
    obj = {
        hello: function (word) {
            return "Hello, " + word;
        }
    };
    var obj2 = void 0;
    obj2 = obj;
    obj = obj2;
}

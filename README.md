# TypeScript の勉強

[Revised Revised 型の国の TypeScript | Revised Revised TypeScript in Definitelyland](http://typescript.ninja/typescript-in-definitelyland/index.html)

## Memo

```
$ npm install -D typescript
$ npx tsc -v
Version 2.9.2
$ npx tsc --init
message TS6071: Successfully created a tsconfig.json file.
$ npx tsc sample.ts
```

`tsconfig.json` の設定を反映させるには、対象ファイルを指定せずに `tsc` コマンドを実行する。

型の宣言空間 (type declaration space)：型定義

値の宣言空間 (variable declaration space)：実装

情報として型が存在していても、値として存在していない、という場面は多い

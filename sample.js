"use strict";
{
    let str = "str";
    let num = 1;
    let bool = true;
    let func = () => { };
    let obj = {};
    console.log(str, num, bool, func(), obj);
    //   str = 1;
    //   num = "str";
    //   bool = null;
}
{
    class Base {
        constructor(str) {
            this.num = 1;
            this.str = str;
        }
        hello() {
            return `Hello ${this.str}`;
        }
        greeting(name) {
            return `Hi! ${name}`;
        }
        get regExp() {
            if (!this.regExpOptional) {
                return new RegExp("test");
            }
            return this.regExpOptional;
        }
    }
    class Inherit extends Base {
        greeting(name) {
            return `${super.greeting(name)}. How are you?`;
        }
    }
    const base = new Base("world");
    const inherit = new Inherit("my world");
    console.log(base.hello());
    console.log(base.greeting("TypeScript"));
    console.log(inherit.greeting("TypeScript"));
}
{
    class Base {
        constructor() {
            this.a = "a";
            this.b = "b";
            this.c = "c";
            this.d = "d";
        }
        method() {
            this.d;
        }
    }
    class Inherit extends Base {
        method() {
            this.a;
            this.b;
            this.c;
            // this.d;
        }
    }
    const base = new Base();
    const inherit = new Inherit();
    console.log(base.a);
    console.log(base.b);
    // console.log(base.c);
    // console.log(base.d);
    console.log(inherit.a);
    console.log(inherit.b);
    // console.log(inherit.c);
    // console.log(inherit.d);
}
{
    class BaseA {
        constructor(str) {
            this.str = str;
            this.str = str;
        }
        method() {
            console.log(this.str);
        }
    }
    class BaseB {
        constructor(str) {
            this.str = str;
        }
    }
    console.log(new BaseA("A"));
    console.log(new BaseB("B"));
}
{
    class Animal {
        sleep() {
            return "zzzZZZ...";
        }
    }
    class Cat extends Animal {
        constructor() {
            super(...arguments);
            this.name = "Cat";
            this.poo = "Poo";
        }
        speak() {
            return "meow";
        }
    }
    console.log(new Cat());
}
{
    function hello(word) {
        return `Hello, ${word}`;
    }
    console.log(hello("world"));
    function bye(word) {
        return `Bye, ${word}`;
    }
    console.log(bye("world"));
    function hey(word) {
        return `Hey, ${word || "TypeScript"}`;
    }
    console.log(hey());
    console.log(hey("world"));
    function ahoy(word = "TypeScript") {
        return `Ahoy! ${word}`;
    }
    console.log(ahoy());
    function funcA(arg1, arg2) {
        return `Hello, ${arg1}, ${arg2}`;
    }
    console.log(funcA("A", "B"));
    function funcB(arg, ...args) {
        return `Hello, ${arg}, ${args.join(".")}`;
    }
    console.log(funcB("A", "B", "C"));
}
{
    function asyncModoki(callback) {
        callback("TypeScript");
    }
    asyncModoki((value) => console.log(`Hello, ${value}`));
}
{
    function returnByPromise(word) {
        return new Promise(resolve => {
            setTimeout(() => {
                resolve(word);
            }, 100);
        });
    }
    async function helloAsync() {
        console.log("A");
        const word = await returnByPromise("B");
        console.log(word);
        console.log("C");
        return "D";
    }
    (async () => {
        const hello = await helloAsync();
        console.log(hello);
    })();
    // helloAsync().then(hello => console.log(hello));
}

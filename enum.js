"use strict";
var Suit;
(function (Suit) {
    Suit[Suit["Heart"] = 0] = "Heart";
    Suit[Suit["DIamond"] = 1] = "DIamond";
    Suit[Suit["Club"] = 2] = "Club";
    Suit[Suit["Spade"] = 3] = "Spade";
})(Suit || (Suit = {}));
console.log(Suit.Heart, Suit[Suit.Heart]);
console.log(7 /* All */);
var Tree;
(function (Tree) {
    Tree["Node"] = "node";
    Tree["Leaf"] = "leaf";
})(Tree || (Tree = {}));
console.log(Tree.Node);

// オブジェクト型リテラルは、JavaScriptのオブジェクトリテラルに似た記法で、匿名の型を作り出す機能です
// オブジェクト型リテラルは型を指定する箇所*1であればどこでも使えます

{
  let objA = {
    x: 1,
    y: 2
  };

  let objB: {
    x: number;
    y: number;
  };
  objB = { x: 1, y: 2 };

  console.log(objA);
  console.log(objB);

  function move(
    value: { x: number; y: number },
    delta: { dx?: number; dy?: number }
  ): { x: number; y: number } {
    if (delta.dx) {
      value.x += delta.dx;
    }
    if (delta.dy) {
      value.y += delta.dy;
    }
    return value;
  }

  let result = move({ x: 1, y: 2 }, { dx: 3 });
  console.log(result);
}

// Property Signatures
{
  let obj: {
    property: string;
  };

  obj = {
    property: "Hi!"
  };

  console.log(obj.property);
}

// Call Signatures
{
  let func: {
    (word: string): string;
  };

  func = (word: string): string => {
    return `Hello ${word}!`;
  };

  console.log(func("world"));
}

// Construct SignaturesZ
{
  let myClass: {
    new (): any;
  };

  myClass = class {};

  console.log(new myClass());
}

// Index Signatures
// インデックスシグニチャの利用は静的な検証の恩恵を受けられない危険性が高いため、安易に使わないようにしましょう。
{
  let objA: {
    [index: number]: string;
  } = {};

  // どういった使い方ができるの？
  let s1 = objA[1];

  let objB: {
    [index: string]: string;
  } = {};

  let s3 = objB[1];
  let s4 = objB["test"];

  objA = {
    0: "str"
  };
  let tmp = {
    0: "str",
    str: "str"
  };
  objA = tmp;

  objB = {
    0: "str",
    str: "str"
  };

  console.log(s1, s3, s4);
}

// Method Signatures
{
  let obj: {
    hello(word: string): string;
  };

  obj = {
    hello(word: string) {
      return `Hello, ${word}`;
    }
  };

  let obj2: {
    hello: (word: string) => string;
  };
  obj2 = obj;
  obj = obj2;
}

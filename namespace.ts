namespace a {
  class Sample {
    hello(word = "TypeScript") {
      return `Hello ${word}`;
    }
  }

  export interface Hello {
    hello(word?: string): string;
  }

  export let obj: Hello = new Sample();
}

namespace b {
  export namespace c {
    export function hello() {
      let objA = a.obj;
      return objA.hello();
    }
  }
}

console.log(b.c.hello());

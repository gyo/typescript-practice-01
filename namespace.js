"use strict";
var a;
(function (a) {
    class Sample {
        hello(word = "TypeScript") {
            return `Hello ${word}`;
        }
    }
    a.obj = new Sample();
})(a || (a = {}));
var b;
(function (b) {
    let c;
    (function (c) {
        function hello() {
            let objA = a.obj;
            return objA.hello();
        }
        c.hello = hello;
    })(c = b.c || (b.c = {}));
})(b || (b = {}));
console.log(b.c.hello());
